from flask import Flask, make_response, request
from pathlib import Path
import git
import subprocess
import stat
import os

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

assert(app.config['REPO_URL'] != '')


def error(err_text, err_code):
    response = make_response(err_text, err_code)
    response.mimetype = "text/plain"
    return response


@app.route('/')
def run():
    if app.config['AUTH_TOKEN'] != "":
        auth_token = request.args.get('token')
        if not auth_token == app.config['AUTH_TOKEN']:
            response = make_response('Bad authentication token', 401)
            response.mimetype = "text/plain"
            return response

    repo_dir = Path(app.config['BASEDIR'], app.config['REPO_DIR'], 'repo')
    try:
        git.Repo(repo_dir)
        print("Repo opened on disk")
        repo_ready = True
    except:
        repo_ready = False
        print("Repo not on disk")

    if not repo_ready:
        try:
            git.Repo.clone_from(app.config['REPO_URL'], repo_dir)
            print("Repo cloned from URL")
        except:
            return error("Error cloning repository", 500)

    repo = git.Repo(repo_dir)
    for remote in repo.remotes:
        try:
            remote.fetch()
        except AssertionError as err:
            return error(err, 500)

    output = "Fetched repo\n\n"

    try:
        os.mkdir("/flask/.ssh", mode=600)
    except:
        pass

    sshkey = open(app.config['ANSIBLE_PRIVATE_KEY_FILE'], mode='w')
    sshkey.write(app.config['ANSIBLE_SSH_KEY'])
    sshkey.close()
    os.chmod(app.config['ANSIBLE_PRIVATE_KEY_FILE'], stat.S_IRWXU)

    requirements_path = Path(repo_dir, app.config['ANSIBLE_REQUIREMENTS_FILE'])
    print("Load requirements from {0}".format(requirements_path))
    subprocess.run(['ansible-galaxy', 'install', '-r', requirements_path])

    cmd = 'cd {0} && {1}'.format(repo_dir, app.config['CMD'])
    result = subprocess.run(cmd, capture_output=True, universal_newlines=True, shell=True)
    print(result.stdout)
    print(result.stderr)
    output = output + result.stdout

    response = make_response(output, 200)
    response.mimetype = "text/plain"
    return response
