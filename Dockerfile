FROM python:3

LABEL maintainer="Bengt Giger <beni@directbox.com>"

ENV ANSIBLE_HOST_KEY_CHECKING=False
ENV ANSIBLE_REMOTE_USER=root
ENV ANSIBLE_INVENTORY=inventory
ENV ANSIBLE_RETRY_FILES_ENABLED=False
ENV ANSIBLE_PRIVATE_KEY_FILE=/flask/.ssh/id_rsa
ENV REPO_BASE_PATH=instance/shared/repo
ENV ANSIBLE_REQUIREMENTS_FILE=roles/requirements.yml

# RUN apt-get update && apt-get -y install ansible
# 
RUN apt-get update && apt-get install -y python3-pip && pip3 install ansible

RUN adduser --system --home /flask --uid 20913 --gid 1 flask
RUN mkdir -p /flask/.ssh && chown -R flask /flask

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 5000
USER flask
ENV FLASK_APP push-server.py
CMD flask run --host=0.0.0.0 --port=5000
